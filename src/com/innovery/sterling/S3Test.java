package com.innovery.sterling;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;

import com.sterlingcommerce.woodstock.workflow.Document;
import com.sterlingcommerce.woodstock.workflow.WorkFlowContext;

public class S3Test {
	
	public static void main(String[] args) {
		WorkFlowContext wfc = new WorkFlowContext();
		wfc.setServiceName("SERVICIO DE PRUEBAS");
		wfc.getServiceParms().setProperty("operation", "PUT");
		wfc.getServiceParms().setProperty("awss3_accessKey", "");
		wfc.getServiceParms().setProperty("awss3_secretKey", "");
		wfc.getServiceParms().setProperty("s3_mEndpoint", "");
		wfc.getServiceParms().setProperty("s3_mRegion", "");
		wfc.getServiceParms().setProperty("s3_tipo", "multipartAltoNivel");
		
		Document primaryDoc = new Document();
		try {
			File f = new File("C:/Santander/Scality/pruebas");
			InputStream input = new FileInputStream(f);
			primaryDoc.setBody(IOUtils.toByteArray(input));
		} catch (Exception e) {
			System.out.println(e);
		}
		wfc.putPrimaryDocument(primaryDoc);
		
		try {
			S3Implementation s3Impl = new S3Implementation();
			s3Impl.processData(wfc);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
