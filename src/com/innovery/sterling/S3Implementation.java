package com.innovery.sterling;


import java.io.ByteArrayInputStream;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import com.sterlingcommerce.woodstock.services.IService;
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.CompleteMultipartUploadRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.InitiateMultipartUploadRequest;
import com.amazonaws.services.s3.model.InitiateMultipartUploadResult;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PartETag;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.UploadPartRequest;
import com.amazonaws.services.s3.model.UploadPartResult;

import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.amazonaws.services.s3.transfer.Upload;
import com.sterlingcommerce.woodstock.services.XLogger;
import com.sterlingcommerce.woodstock.workflow.Document;
import com.sterlingcommerce.woodstock.workflow.WFCBase;
import com.sterlingcommerce.woodstock.workflow.WorkFlowContext;
import com.sterlingcommerce.woodstock.workflow.WorkFlowException;
import com.sterlingcommerce.woodstock.services.controller.ServicesControllerImpl;


public class S3Implementation implements IService {
   
    private Document primaryDoc = null;
   
    private InputStream docAsStream = null;
   
    private static boolean isOK = true;
        private static final String SUFFIX = "/";
   
       
    public WorkFlowContext processData(final WorkFlowContext wfc)throws WorkFlowException, UnsupportedOperationException {

        final String svcName = wfc.getServiceName();
        final XLogger log = new XLogger("CUSTOM_PRODUBAN_S3", svcName,"CUSTOM_PRODUBAN_S3");
        ServicesControllerImpl sci = ServicesControllerImpl.getInstance();
        sci.harnessRegister(String.valueOf(wfc.getWorkFlowId()), svcName);
        wfc.setBasicStatus(WorkFlowContext.SUCCESS); 

        try {
            isOK = true;
          
            log.log("************* Start custom ****************");
            wfc.harnessRegister();
            final String action = wfc.getParm("operation");//PUT, GET, DELETE, MKDIR, BUCKETS, FILTER
            final String accessKey = wfc.getParm("awss3_accessKey"); // accessKey S3
            final String secretKey = wfc.getParm("awss3_secretKey"); // secrectKey S3
            final String mEndpoint = wfc.getParm("s3_mEndpoint"); // mEndpoint
            final String mRegion = wfc.getParm("s3_mRegion"); //mRegion S3
            
            final String tipo = wfc.getParm("s3_tipo"); //multipartAltoNivel, multipartBajoNivel, cargaSimple
          
            String s3_bucketname = null;//Bucket name in S3 area
            String s3_key = null;  //Name with which resource resides in S3 area
            String s3_folderName = "";//Folder name OR structure
           
            String filter = null; //Filter an determinate name of keys in one bucket
           
                //String local_path = wfc.getParm("local_path");//Path to read file from for uploading
                //String messageId = wfc.getParm("messageId");//Message ID for mailbox
 
    
            //log.log("AWSS3ClientService.processData(): s3_key:" + s3_key);
            log.log("AWSS3ClientService.processData(): action:" + action);
         
           // log.log("AWSS3ClientService.processData(): s3_bucketname:" + s3_bucketname);
            //log.log("AWSS3ClientService.processData(): s3_folderName:" + s3_folderName);
           // log.log("AWSS3ClientService.processData(): AccessKey for AWS S3 area: " + accessKey);
           // log.log("AWSS3ClientService.processData(): SecretKey for AWS S3 area: " + secretKey);
                      
            final BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey,secretKey); // secret key
           
            log.log("AWSS3ClientService.awsConfig()");
            final EndpointConfiguration awsConfig = new AwsClientBuilder.EndpointConfiguration (mEndpoint, mRegion);
           
            // create a client connection based on credentials
            log.log("AWSS3ClientService.s3Client()");
            final AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(awsCreds)).withEndpointConfiguration(awsConfig).build();

            if ("PUT".equalsIgnoreCase(action)) {
            	 log.log("AWSS3ClientService.PUT()");
                 if(wfc.getPrimaryDocument() != null){
                     s3_bucketname = wfc.getParm("s3_bucketname");
                     s3_key = wfc.getParm("s3_key");
                     String limitSize = wfc.getParm("limit_size").trim();
                     docAsStream = wfc.getPrimaryDocument().getBodyInputStream();
                     long docSize = wfc.getPrimaryDocument().getSize();
                     
                     if(tipo!=null){
                    	 //Gestionamos el tipo de subida del fichero. Carga simple o multipart de bajo o alto nivel.
                    	 if(tipo.equals("multipartAltoNivel")){
                    		 multipartAltoNivel(docAsStream, s3_key, s3Client, s3_bucketname, log, wfc);
                         }else if(tipo.equals("multipartBajoNivel")){
                        	 multipartBajoNivel(docAsStream, s3_key, s3Client, s3_bucketname, log, wfc, docSize);
                         }else if(tipo.equals("cargaSimple")){
                        	 cargaSimple(docAsStream, s3_key, s3Client, s3_bucketname, log, wfc, limitSize);
                         }else{
                 	    	isOK = false;
                	    	wfc.addWFContent("ErrorCause", "El tipo de carga especificada (s3_tipo) no existe");
                	    	log.log("ERROR en catch: " + "El tipo de carga especificada (s3_tipo) no existe");
                         }
                     }else{
                    	 isOK = false;
                    	 wfc.addWFContent("ErrorCause", "Se debe especificar el tipo de carga (s3_tipo)");
                    	 log.log("ERROR: " + "Se debe especificar el tipo de carga (s3_tipo)");
                     }
                     
                     

                     log.log("AWSS3ClientService.finish putFileToS3()");
                 } else {
                     isOK = false;
                     wfc.addWFContent("ErrorCause", "Primary Document empty");
                 }
                
               // else if (local_path != null && !"".equalsIgnoreCase(local_path)) {// put file from FS to S3
               //     docAsStream = new FileInputStream( new File( local_path ));
               //     putFileToS3(docAsStream, s3_key, s3Client, s3_bucketname);
               // }
               // } else if (messageId != null && !"".equalsIgnoreCase(messageId) && !"null".equalsIgnoreCase(messageId)) {
               //     putFileFromMailboxToS3(messageId, s3_key, s3Client, s3_bucketname);
               // }

            } else if ("GET".equalsIgnoreCase(action)) {
                log.log("Usage: s3client read <s3_key>");
                s3_bucketname = wfc.getParm("s3_bucketname");
                s3_key = wfc.getParm("s3_key");
               
                primaryDoc = readFileFromS3(s3_key, s3Client, s3_bucketname, log);
                wfc.putPrimaryDocument(primaryDoc);
            } else if ("DELETE".equalsIgnoreCase(action)) {
                log.log("Usage: s3client delete <s3_key>");
                s3_bucketname = wfc.getParm("s3_bucketname");
                s3_key = wfc.getParm("s3_key");
                s3_folderName = wfc.getParm("s3_foldername");
               
                delete(s3_bucketname, s3_folderName, s3Client, s3_key, log, wfc);
            } else if ("MKDIR".equalsIgnoreCase(action)) {// create folder into bucket
                log.log("Usage: s3client mkdir <s3_key>");
                s3_bucketname = wfc.getParm("s3_bucketname");
                s3_folderName = wfc.getParm("s3_foldername");
                createFolder(s3_bucketname, s3_folderName, s3Client, log);
               
            } else if ("BUCKETS".equalsIgnoreCase(action)) {// list folders of buckets
                log.log("Usage: s3client list <s3_key>");
                listBuckets(s3Client,log,wfc);
                wfc.putPrimaryDocument(primaryDoc);
            } else if ("FILTER".equalsIgnoreCase(action)) {// list folders
                log.log("Usage: s3client filter <s3_key>");
                s3_bucketname = wfc.getParm("s3_bucketname");
                filter = wfc.getParm("filterkey");
               
                filterKeys(s3Client,filter,s3_bucketname,log,wfc);
                wfc.putPrimaryDocument(primaryDoc);
            } else {
                log.log("Usage: s3client PUT,GET,DELETE,MKDIR,LIST,FILTER" + " [<local_path> <s3_key>]");
                wfc.addWFContent("ErrorCause","Usage: s3client PUT,GET,DELETE,MKDIR,LIST,FILTER");
                isOK = false;
                //System.exit(1);
            }

            log.log("Done!");
           
         } catch (final Exception e) {
                isOK = false;
                wfc.addWFContent("ErrorCause",e.toString());
                log.log("ERROR en catch: " +e.getMessage());
                log.logException(svcName, e);
               
                //handleError(log, wfc, S3Messages.Global_Exception,new Object[] { S3Implementation.class.getName() }, e);
               
            } catch (final Throwable throwable) {
                isOK = false;
                wfc.addWFContent("ErrorCause",throwable.toString());
                log.log("ERROR en catch: " +throwable.getMessage());
                log.logException(svcName, throwable);
               
                //handleError(log, wfc, S3Messages.Global_Exception,new Object[] { S3Implementation.class.getName() }, throwable);
               
        } finally {
            sci.unregisterThread();
            //wfc.unregisterThread();
            if (!isOK) {       
            wfc.addWFContent("ResultS3","KO");
            wfc.setBasicStatus(WFCBase.ERROR);
            log.log("************* Finish custom ****************");
            }else{
            wfc.addWFContent("ResultS3","OK");
            log.log("************* Finish custom ****************");
            }
        }
        return wfc;
    }

        public void createFolder(final String bucketName, final String folderName, final AmazonS3 client, final XLogger log) {
                // create meta-data for your folder and set content-length to 0
                final ObjectMetadata metadata = new ObjectMetadata();
                metadata.setContentLength(0);

                // create empty content
                final InputStream emptyContent = new ByteArrayInputStream(new byte[0]);

                // create a PutObjectRequest passing the folder name suffixed by /
                final PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName,
                                folderName + SUFFIX, emptyContent, metadata);

                // send request to S3 to create folder
                client.putObject(putObjectRequest);
        }
     
    public void cargaSimple(InputStream source, String key, AmazonS3 s3client, String bucketName, XLogger log, WorkFlowContext wfc, String limitSize)
    throws IOException{
    	/*
    	 * M�todo que gestiona la carga simple de un fichero
    	 * Ficheros de hasta 5GB
    	 * */
	    try{
	        log.log("AWSS3ClientService.resultPut()");
	        int limitSizeInt = Integer.parseInt(limitSize);
	      
	        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, key, source, null);
	        putObjectRequest.getRequestClientOptions().setReadLimit(limitSizeInt);
	      
	        s3client.putObject(putObjectRequest);
	    }catch (AmazonClientException e){
	    	isOK = false;
	    	wfc.addWFContent("ErrorCause", e.toString());
	    	log.log("ERROR en catch: " + e.getMessage());
	    	log.logException("Error putFileToS3", e);
	    }
    }
    
    public void multipartAltoNivel(InputStream source, String key, AmazonS3 s3client, String bucketName, XLogger log, WorkFlowContext wfc)
    throws Exception{
    	/*
    	 * M�todo que gestiona la subida del fichero como multipart de alto nivel.
    	 * Ficheros de hasta 5TB
    	 * */
        try {
            TransferManager tm = TransferManagerBuilder.standard()
                    .withS3Client(s3client)
                    .build();
            // TransferManager processes all transfers asynchronously,
            // so this call returns immediately.
	        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, key, source, null);
            Upload upload = tm.upload(putObjectRequest);
            System.out.println("Object upload started");
            // Optionally, wait for the upload to finish before continuing.
            upload.waitForCompletion();
            System.out.println("Object upload complete");
        } catch (AmazonServiceException e) {
            // The call was transmitted successfully, but Amazon S3 couldn't process 
            // it, so it returned an error response.
	    	isOK = false;
	    	wfc.addWFContent("Error multipartAltoNivel", e.toString());
	    	log.log("ERROR en multipartAltoNivel: " + e.getMessage());
	    	log.logException("Error multipartAltoNivel", e);
        } catch (SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
	    	isOK = false;
	    	wfc.addWFContent("Error multipartAltoNivel", e.toString());
	    	log.log("ERROR en multipartAltoNivel: " + e.getMessage());
	    	log.logException("Error multipartAltoNivel", e);
        }
    }
    
    public void multipartBajoNivel(InputStream source, String key, AmazonS3 s3client, String bucketName, XLogger log, WorkFlowContext wfc, long contentLength)
    throws Exception{
    	/*
    	 * M�todo que gestiona la subida del fichero como multipart de bajo nivel. 
    	 * Divide el fichero en partes m�s peque�as (partSize) y las va subiendo hasta completar el fichero entero de un tama�o contentLength.
    	 * Ficheros de hasta 5TB
    	 * */
        long partSize = 5 * 1024 * 1024; // Set part size to 5 MB. 

        try {

            // Create a list of ETag objects. You retrieve ETags for each object part uploaded,
            // then, after each individual part has been uploaded, pass the list of ETags to 
            // the request to complete the upload.
            List<PartETag> partETags = new ArrayList<PartETag>();

            // Initiate the multipart upload.
            InitiateMultipartUploadRequest initRequest = new InitiateMultipartUploadRequest(bucketName, key);
            InitiateMultipartUploadResult initResponse = s3client.initiateMultipartUpload(initRequest);

            // Upload the file parts.
            long filePosition = 0;
            for (int i = 1; filePosition < contentLength; i++) {
                // Because the last part could be less than 5 MB, adjust the part size as needed.
                partSize = Math.min(partSize, (contentLength - filePosition));
                // Create the request to upload a part.
                UploadPartRequest uploadRequest = new UploadPartRequest()
                        .withBucketName(bucketName)
                        .withKey(key)
                        .withUploadId(initResponse.getUploadId())
                        .withPartNumber(i)
                        .withFileOffset(filePosition)
                        .withInputStream(source)
                        .withPartSize(partSize);

                // Upload the part and add the response's ETag to our list.
                UploadPartResult uploadResult = s3client.uploadPart(uploadRequest);
                partETags.add(uploadResult.getPartETag());

                filePosition += partSize;
            }

            // Complete the multipart upload.
            CompleteMultipartUploadRequest compRequest = new CompleteMultipartUploadRequest(bucketName, key,
                    initResponse.getUploadId(), partETags);
            s3client.completeMultipartUpload(compRequest);
        } catch (AmazonServiceException e) {
            // The call was transmitted successfully, but Amazon S3 couldn't process 
            // it, so it returned an error response.
	    	isOK = false;
	    	wfc.addWFContent("Error multipartBajoNivel", e.toString());
	    	log.log("ERROR en multipartBajoNivel: " + e.getMessage());
	    	log.logException("Error multipartBajoNivel", e);
        } catch (SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
	    	isOK = false;
	    	wfc.addWFContent("Error multipartBajoNivel", e.toString());
	    	log.log("ERROR en multipartBajoNivel: " + e.getMessage());
	    	log.logException("Error multipartBajoNivel", e);
        }
    }
/**
    public void putFileFromMailboxToS3(String messageId, String key, AmazonS3 s3client, String bucketName)
            throws IOException {       
            IRepository repos = MailboxFactory.getRepository();
            String filename = null;
            Message message = null;
            Document batchDoc = null;
            try {
                log.log("AWSS3Client.putFileFromMailboxToS3():messageId:" + messageId);
                message = repos.getMessage(Long.parseLong(messageId));// source here is messageid
                log.log("AWSS3Client.putFileFromMailboxToS3(): message:" + message.toString());
                String docId = message.getDocumentId();
                log.log("AWSS3Client.putFileFromMailboxToS3(): docId:" + docId);
                filename = message.getMessageName();
                log.log("AWSS3Client.putFileFromMailboxToS3():filename:" + filename);
                if (docId != null) {
                    batchDoc = new Document(docId, true);
                }
                log.log("AWSS3Client.putFileFromMailboxToS3(): batchDoc:" + batchDoc);
            } catch (NumberFormatException nfe) {
                log.logException(nfe);
            } catch (MailboxException me) {
                log.logException(me);
            } catch (DocumentNotFoundException dnfe) {
                log.logException(dnfe);
            } catch (SQLException sqle) {
                log.logException(sqle);
            }

            InputStream in = batchDoc.getInputStream();
            final File aFile = File.createTempFile("tmp", "txt");
            aFile.deleteOnExit();
            try{
               FileOutputStream out = new FileOutputStream(aFile);
               IOUtils.copy(in, out);
            } catch (IOException ioe) {
                log.logException(ioe);
            }
                s3client.putObject(new PutObjectRequest(bucketName, key, aFile).withCannedAcl(CannedAccessControlList.PublicRead));
    }
*/
    public Document readFileFromS3(String key,AmazonS3 s3Client,String bucketName,XLogger log) throws IOException {

	    org.apache.commons.io.output.ByteArrayOutputStream finalTemp = new org.apache.commons.io.output.ByteArrayOutputStream();
        S3Object object = s3Client.getObject(new GetObjectRequest(bucketName, key));
        InputStream inp = object.getObjectContent();
        	log.log("AWSS3ClientService.readFileFromS3()");
        String filename = key.substring(key.lastIndexOf('/') + 1, key.length());
  
	    finalTemp.write(inp);
        inp.close();

        Document primaryDocTemp = new Document();
        primaryDocTemp.setBody(finalTemp.toByteArray());
        
        finalTemp.close();
        primaryDocTemp.setBodyName(filename);
        
        return primaryDocTemp;
        
    }


        /**
         * If key is null - This method first deletes all the files in given folder and then the folder itself
         * If key is not null - This method deletes the given fileName
         */
        public void delete(final String bucketName, final String folderName, final AmazonS3 client, final String fileName,final XLogger log,final WorkFlowContext wfc) {
           
                final List<S3ObjectSummary> fileList =
                                client.listObjects(bucketName, folderName).getObjectSummaries();
                if(fileName!=null){
                        for (final S3ObjectSummary file : fileList) {
                                log.log("AWSS3ClientMain.delete(): fileName:"+fileName);
                                log.log("AWSS3ClientMain.delete(): file.getKey():"+file.getKey());
                                if (file.getKey().contains(fileName)) {
                                        client.deleteObject(bucketName, file.getKey());
                                }
                        }
                }else{
                        log.log("AWSS3ClientMain.delete(): fileName:"+fileName);
                        for (final S3ObjectSummary file : fileList) {
                                client.deleteObject(bucketName, file.getKey());
                        }
                        client.deleteObject(bucketName, folderName);
                }

        }
       
        // Actualmente no se va a usar ya que s�lo hay un bucket
        // list buckets
       
        public void listBuckets(final AmazonS3 s3Client,final XLogger log,final WorkFlowContext wfc){
           
             int TokenCount = 0;
             for (final Bucket bucket : s3Client.listBuckets()) {
             TokenCount++;
             wfc.setWFContent( "Bucket" + String.valueOf(TokenCount), bucket.getName() );
             }
        }
       
        //Filter files
        public void filterKeys(final AmazonS3 s3Client,final String filter, final String bucketName,final XLogger log,final WorkFlowContext wfc){
           
            ObjectListing objectListing = s3Client.listObjects(new ListObjectsRequest().withBucketName(bucketName));
            int TokenCount = 0;
            String key = null;
             
            for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
               
                //log.log("filterKeys** estoy en filterKeys"+filter);
                if (filter=="" || filter == null){
                    TokenCount++;
                    key = objectSummary.getKey();
                    //log.log("estoy en filterKeys en vacio y ahora hago un setWFContent "+filter+" y la key es: "+key);
                    wfc.setWFContent( "S3Key" + String.valueOf(TokenCount),key);
                     
                }else{
                    key = objectSummary.getKey();
                    if( key.contains(filter)){
                        TokenCount++;
                        //log.log("estoy en filterKeys en lleno y ahora hago un setWFContent "+filter+" y la key es: "+key);
                        wfc.setWFContent( "S3Key" + String.valueOf(TokenCount), key);
                     }
                }
            }
        }
       
        private void handleError(final XLogger log, final WorkFlowContext wfc, final String msg,
                final Object[] parms, final Throwable e) {
            final String newMsg = MessageFormat.format(msg, parms);
            log.logException(newMsg, e);
            setStatus(wfc, WorkFlowContext.ERROR, e.getMessage());
        }

        private void setStatus(final WorkFlowContext wfc, final int basicStatus,
                final String advStatus) {
            wfc.setBasicStatus(basicStatus);
            if (advStatus != null) {
                wfc.setAdvancedStatus(advStatus);
            }
        }

}