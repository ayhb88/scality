package com.innovery.sterling;

import com.sterlingcommerce.woodstock.services.NLS;

public class S3Messages extends NLS {
	private static final String BUNDLE_NAME = "com.innovery.sterling.S3ConectionResources";

	/**
	 * 
	 */

	public S3Messages() {
	}

	static {
		NLS.initializeMessages(BUNDLE_NAME, S3Messages.class);
	}

	// Global messsages
	public static String Global_Exception;

	// Message for CUSTOM_CONDICIONES_DE_SALIDA_ONEFORMATImpl
	public static String S3Implementation_RMINotFound;
}
